<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Smart Wizard 2 - Basic Example  - a javascript jQuery wizard control plugin</title>
 <%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
 <%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
  <%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<link href="stylesheet/smart_wizard.css" rel="stylesheet" type="text/css">
<link href="stylesheet/setup.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.smartWizard-2.0.min.js"></script>

<script type="text/javascript">
	var captchaTest;
    $(document).ready(function(){
    	// Smart Wizard 	
  		$('#wizard').smartWizard({transitionEffect:'slideleft',onFinish:onFinishCallback,onLeaveStep:leaveAStepCallback});
  		$('.editable').click(function(){
  			$($(this).attr('href')).removeAttr('readonly');
  		});
      	$('.elements').blur(function(){
      		$(this).attr('readonly','readonly');
      	});
      function leaveAStepCallback(obj){
      	var step_num= obj.attr('rel');
      	$('#wizard').smartWizard('setError',{stepnum:step_num,iserror:false});
      	return true;
      }
      function onFinishCallback(){
       	alert("it is finished");
       	if(captchaTest == $('#captchaVerify').val())
       	{
       		alert('ok');
       	}
       	else
       	{
       		alert('not');
       	}
      }   
      	test1 = Math.floor(Math.random() *11);
      	test2 = Math.floor(Math.random() *11);
      	captchaTest = test1 + test2;
      	$('#captchaTest1').html(test1);
      	$('#captchaTest2').html(test2);
		});
</script>

</head><body>

<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td> 
<!-- Smart Wizard -->
		<form action = "setup.do">
  		<div id="wizard" class="swMain">
  			<ul>
  				<li><a href="#step-1">
                <span class="stepNumber">1</span>
                <span class="stepDesc">
                   Step 1<br />
                   <small>Personal Contacts</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <span class="stepNumber">2</span>
                <span class="stepDesc">
                   Step 2<br />
                   <small>Profile Information</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <span class="stepNumber">3</span>
                <span class="stepDesc">
                   Step 3<br />
                   <small>Check your whole details</small>
                </span>                   
             </a></li>
  			</ul>
  			<div id="step-1">	
            <h2 class="StepTitle">Personal Contacts</h2>
            <table class = "setUpTable">
            	<tr>
            		<td>Enter your HomeTown :</td>
            		<td><input type = "text" name = "native"></td>
            	</tr>
            	<tr>
            		<td>Enter your CurrentCity :</td>
            		<td><input type = "text" name = "current"></td>
            	</tr>
            	<tr>
            		<td>Enter your MobileNumber :</td>
            		<td><input type = "text" name = "mobile"></td>
            	</tr>
            </table>
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Profile Information</h2>
            <table class = "setUpTable" cellpadding = "15px">
            	<tr>
            		<td>Profile photo</td>
            		<td>
				  <div id = "profileDiv">
  	            		<logic:equal name = "signUpForm" property = "gender" value = "Male">
				  		<img src = "/CronyPlus/images/Default_Profile_Photo/male.gif" style = "width:100px; height:100px;" >
	            		</logic:equal>
					<logic:equal name = "signUpForm" property = "gender" value = "FeMale">
				  		<img src = "/CronyPlus/images/Default_Profile_Photo/female.jpg" style = "width:100px; height:100px;" >
	            		</logic:equal>	            		
				  </div>
			  	</td>
			  	<td>
			  		<input type = "file" >
			  	</td>
		  	</tr>
		  	<tr>
		  		<td>loginName :</td>
            		<td>
            			<input type = "text" name = "loginName"></td>
	       	</tr>
            </table>
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Please verify as you are human.</h2>	
            <table cellpadding = 10px>
            	<tr>
            		<td>
            			First Name :
            		</td>
            		<td>
            			<html:text name = "signUpForm" property = "firstName" readonly = "true" styleId = "firstName" styleClass = "elements"/>
            		</td>
            		<td><a class = "editable" href = "#firstName">Edit</a></td>
            	</tr>
            	<tr>
            		<td>
            			Last Name :
            		</td>
            		<td>
            			<html:text name = "signUpForm" property = "lastName" readonly = "true" styleId = "lastName" styleClass = "elements"/>
            		</td>
            		<td><a class = "editable" href = "#lastName">Edit</a></td>
            	</tr>
            	<tr>
            		<td>
            			Email-Id :
            		</td>
            		<td>
            			<html:text name = "signUpForm" property = "email" readonly = "true" styleId = "email" styleClass = "elements"/>
            		</td>
            		<td><a class = "editable" href = "#email">Edit</a></td>
            	</tr>
            	<tr>
            		<td>
            			Gender :
            		</td>
            		<td>
            			<html:text name = "signUpForm" property = "gender" readonly = "true" styleId = "gender" styleClass = "elements"/>
            		</td>
            		<td><a class = "editable" href = "#gender">Edit</a></td>
            	</tr>
            	<tr>
            		<td>
            			Date Of Birth :
            		</td>
            		<td>
            			<html:text name = "signUpForm" property = "dob" readonly = "true" styleId = "dob" styleClass = "elements"/>
            		</td>
            		<td><a class = "editable" href = "#dob">Edit</a></td>
            	</tr>
            </table>
             <div id = "captchaDiv">
             Captcha Verify
            <table>
            	<tr>
            		<td>
           			<div id="captchaTest1"></div>
           		</td>
           		<td>
           			+
           		</td>
           		<td>
           			<div id="captchaTest2"</div>
           		</td>
           		<td>
           			=
           		</td>
           		<td>
           			<input type=text id="captchaVerify"/>
           		</td>
           	</tr>
           </table>
           </div>
        </div>
  		</div>
  		</form>
<!-- End SmartWizard Content -->  		
 		
</td></tr>
</table>
    		
</body>
</html>
