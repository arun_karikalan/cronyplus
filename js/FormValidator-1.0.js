/* This plug-in is used to validate your  HTML form */
(function( $ ) {
 	
 	$.fn.validate = function(options){
 		var isValid = true;
 		$(".required").hide();
 		$(".error").hide();
 		$(".weakError").hide();
 		$(".lengthError").hide();
 		var form = $(this);
 		var btn = this.find(":submit").prop("type","button");
 		btn.click(function(){
 			form.find(":input:text").each(function(){
 					var valueName = $(this).attr("name");
 					if($('#'+valueName).html() != undefined)
 					{
 						if(this.value == "" || this.value == null)
 						{
	 						isValid = false;
	 						$($('#'+valueName).children(".required")).show();
 						}
						var str = this.value;
						var res = /[^a-zA-Z0-9]/g.test(str);
 						if(res)
 						{
 							isValid = false;
	 						$($('#'+valueName).children(".error")).show();
 						}
 						if(!isValid)
 						{
	 						$(this).focus(function(){
		 							isValid = true;
							 		$($('#'+valueName).children(".required")).hide();
 			 						$($('#'+valueName).children(".error")).hide();
							});
						}
 					}
 				});
 				form.find(":input:password").each(function(){
 					var valueName = $(this).attr("name");
 					if($('#'+valueName).html() != undefined)
 					{
 						if(this.value == "" || this.value == null)
 						{
	 						isValid = false;
	 						$($('#'+valueName).children(".required")).show();
 						}
 						if(7>=this.value.length || this.value.length >= 32)
 						{
 							isValid = false;
	 						$($('#'+valueName).children(".lengthError")).show();
 						}
 						if(!/[0-9]/g.test(this.value) || !/[a-z]/g.test(this.value))
 						{
 							isValid = false;
	 						$($('#'+valueName).children(".weakError")).show();
 						}
 						if(!isValid)
 						{
	 						$(this).focus(function(){
		 							isValid = true;
							 		$($('#'+valueName).children(".required")).hide();
 			 						$($('#'+valueName).children(".lengthError")).hide();
 			 						$($('#'+valueName).children(".weakError")).hide();
							});
						}
 					}
 				});
 				if(isValid)
 				{
 					form.submit();
 				}
 			});
	 	}
}( jQuery ));
