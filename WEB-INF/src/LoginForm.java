package app;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.*;
public class LoginForm extends ActionForm
{
	private String loginName,password,ip;
	public void setLoginName(String loginName)
	{
		this.loginName = loginName.trim();
	}
	public String getLoginName()
	{
		return loginName;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return password;
	}
	public void setIp(String ip)
	{
		this.ip = ip;
	}
	public String getIp()
	{
		return ip;
	}
}
