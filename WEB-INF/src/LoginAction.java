package app;
import java.sql.*;
import app.LoginForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.*;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionMessage;
public class LoginAction extends Action
{ 
	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception
	{
		LoginForm frm = (LoginForm)form;
		ActionMessages Msg = new ActionMessages();
	try
		{
			String query;
			PreparedStatement pStm;
			ResultSet details;
			Class.forName("com.mysql.jdbc.Driver");
		     Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Crony_Plus","root","");
		     if((frm.getLoginName()).contains("@"))
		     {
		     	query = "select * from Person where Email like \""+frm.getLoginName()+"\";";
		     }
		     else
		     {
		     	query = "select * from Person where Login_Name like \""+frm.getLoginName()+"\";";
		     }
		     pStm = con.prepareStatement(query);
		     details = pStm.executeQuery();
		     boolean validEmail = false;
		     boolean validPassword = false;
		     while(details.next())
		     {
		     	validEmail = true;
		     	String pwd = details.getString("Password");
		     	if(pwd.equals(frm.getPassword()))
		     	{
		     		validPassword = true;
		     		String ip = request.getRemoteAddr();
		     		frm.setIp(ip);
			     	return mapping.findForward("success");
		     	}
		     }
		     if(!validEmail)
		     {
		     	Msg.add("loginName",new ActionMessage("error.mail.invalid"));
		     }
		       if(!validPassword)
		     {
		     	Msg.add("password",new ActionMessage("error.password.invalid"));
		     }
		     addErrors(request,Msg);
		     request.getSession().invalidate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return mapping.findForward("failure");
	}
}
