package app;
import java.sql.*;
import app.SignUpForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.*;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionMessage;
public class SignUpAction extends Action
{ 
	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception
	{
		SignUpForm frm = (SignUpForm)form;
		ActionMessages msg = new ActionMessages();
		String query,photo = null;
		String gender = null;
		boolean isValidPerson = true;
		PreparedStatement pStm;
		ResultSet person;
		Class.forName("com.mysql.jdbc.Driver");
	     Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Crony_Plus","root","");
	     if(frm.getGender().equals("Male"))
	     {
	     	gender = "M";
	     	photo = "/CronyPlus/images/Default_Profile_Photo/male.gif";
	     }
	     else if(frm.getGender().equals("FeMale"))
	     {
	     	gender = "F";
	     	photo = "/CronyPlus/images/Default_Profile_Photo/female.jpg";
	     }
	     String checkQry = "select * from Person where Email like \""+frm.getEmail()+"\";";
	     pStm = con.prepareStatement(checkQry);
	     person = pStm.executeQuery();
	     while(person.next())
	     {
	     	isValidPerson = false;
	     } 
	     if(isValidPerson)
	     {
			query = "insert into Person(First_Name,Last_Name,Gender,DOB,Email,Password,Photo,isConfirmed) values (\""+frm.getFirstName()+"\",\""+frm.getLastName()+"\",\""+gender+"\",\""+frm.getDob()+"\",\""+frm.getEmail()+"\",\""+frm.getPassword()+"\",\""+photo+"\",\"n\")";
			pStm = con.prepareStatement(query);
			pStm.executeUpdate(); 
			return mapping.findForward("success");
		}
		else
		{
			msg.add("email",new ActionMessage("error.mail.exists"));
			addErrors(request,msg);
			return mapping.getInputForward();
		}
	}
}
