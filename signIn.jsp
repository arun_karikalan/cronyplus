<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<logic:present name = "loginForm" scope = "session" property = "loginName">
		<logic:redirect href = "home.do"/>
</logic:present>
<html>
	<head>
		<title>Welcome to Crony Plus-Login</title>
		<link rel = "shortcut icon" href = "/CronyPlus/images/CronyPlusIcon.png">
		<link rel="stylesheet" href="/CronyPlus/stylesheet/Index.css">
		<link href="stylesheet/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
		<script src = "/CronyPlus/js/encript.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script>
			window.history.forward();
			$(document).ready(function() {
				$("#firstName").attr("placeholder", " First Name");
				$('#lastName').attr("placeholder", " Last Name");
				$("#email").attr("placeholder", " Enter Your E-mail Id").attr("size",45);
				$('#re-Email').attr("placeholder", " Re-Enter Your E-mail Id").attr("size",45);
				$("#password").attr("placeholder", " Enter new Password").attr("size",45);
				$('#datepicker').attr("placeholder", " Date Of Birth").attr("size",45);
				if($('#errorDiv').html() == "")
				{
					$('#errorDiv').hide();
				}
				$('#signInForm').submit(function(){
					var password = $('#signInPassword').val();
					var hash = des(password,password,0);
					$('#signInPassword').val(stringToHex(hash));
				});	
				$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
				$('#signup-btn').click(function(){
					isValid = true;
					if($('#re-Email').val() == "" || $('#email').val() == "")
					{
						isValid = false;
						$('#errorDiv').html("You Must Fill all The Fields");
					}
					else if($('#re-Email').val() != $('#email').val())
					{
						isValid = false;
						$('#errorDiv').html("Your email id does not match.");
					}
					else if(/[^a-zA-Z0-9@.]/g.test($('#re-Email').val()) || /[^a-zA-Z0-9@.]/g.test($('#email').val()))
					{
						isValid = false;
						$('#errorDiv').html("Your email id is invalid");
					}
					if($('#password').val() == "")
					{
						isValid = false;
						$('#errorDiv').html("You Must Fill all The Fields");
					}
					else if($('#password').val().length <= 7 || $('#password').val().length > 32)
					{
						isValid = false;
						$('#errorDiv').html("Your password should contains 7-32 characters");
					}
					if(!($('#male').is(":checked") || $('#female').is(":checked")))
					{
						isValid = false;
						$('#errorDiv').html('');
				   		$('#errorDiv').html("You must fill either Male or Female");
					}
					$("#sign-UpForm").find(":text").each(function() {
						if(this.value === "" || this.value === null)
						{
							isValid = false;
							$('#errorDiv').html("You Must Fill all The Feilds");
						}
						else if(/[^a-zA-Z0-9]/g.test(this.value) && this.id != "datepicker")
						{	
							isValid = false;
							$('#errorDiv').html("Invalid Entries Found ");
						}
					});
					if(!isValid)
					{	
							$('#errorDiv').show();
					}
					else
					{
						password = $('#password').val();
						var hash = des(password,password,0);
						$('#password').val(stringToHex(hash));
						$('#sign-UpForm').submit();
					}
				});
			});
		</script>
	</head>
	<body>
		<div id="displyDiv">
			<img src="/CronyPlus/images/CronyPlusLogo.png" id="logoImg"/>
			<table id='signInModule'>
				<html:form action="home.do" method="post" styleId = "signInForm">
					<tr>
						<td>Email-Id </td>
						<td>Password</td>
					</tr>
					<tr cellpadding="4px">
						<td><input type="text" name="loginName"></td>
						<td><input type="password" name="password" id = "signInPassword"></td>
						<td></td><td></td>
						<td><input type="submit" class="btn btn-primary" id="signin-btn" value="Sign In"></td>
					</tr>
					<tr>
						<td></td>
						<td><a href="http://localhost:8080">Forgot your password?</a></td>
					</tr>
				</html:form>
			</table>
		</div>
		<div id="sign-Up">
			<html:form action="signUp.do" method="post" styleId="sign-UpForm">
				<table cellpadding = 14px>
					<tr>
						<td><h1><i>Sign Up</i></h1></td>
					</tr>
					<tr>
						<td>WelCome To Crony+</td>
					</tr>
					<tr>
						<td><html:text  name="signUpForm" styleClass="name-widgets" styleId="firstName" property = "firstName"/></td>
						<td><html:text name="signUpForm" styleClass="name-widgets" styleId= "lastName" property = "lastName"/></td>
					</tr>
					<tr>
						<td colspan = 2><input type = "email" name="email" class="signup-widgets" id="email" ></td>
					</tr>
					<tr>
						<td colspan = 2><input type = "email" class="signup-widgets" id="re-Email"></td>
					</tr>
					<tr>
						<td colspan = 2><input type= "password" name="password" class="signup-widgets" id = "password"></td>
					</tr>
					<tr>
						<td colspan = 2><html:text name="signUpForm" styleClass="signup-widgets"  styleId="datepicker"  property = "dob"/></td>
					</tr>
					<tr>
						<td><b>Gender</b></td>
						<td></td>
					</tr>
					<tr>
						<td><input type="radio" name="gender" value = "Male"  id = "male" required>&nbsp &nbsp &nbsp &nbsp Male</td>
						<td><input type="radio" name="gender" value = "FeMale" id = "female"required >&nbsp &nbsp &nbsp &nbsp FeMale</td>
					</tr>
				</table>
					<input type = "button" class="btn btn-success" id="signup-btn" value="signUp">
				</html:form>
				<div id = "errorDiv"><html:errors property = "email"/></div>
		</div>
		<img src = "/CronyPlus/images/signUpAd.jpg" id = "signUpAd">
	</body>
</html>
