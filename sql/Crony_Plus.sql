drop database if exists Crony_Plus;
create database Crony_Plus;
use Crony_Plus;

Create table Person(Id int primary key auto_increment,First_Name varchar(20),Last_Name varchar(20),Login_Name varchar(50),Gender enum('M','F'),DOB date,Email varchar(50),Password varchar(1000),Photo longtext,Mobile_Number varchar(12),isConfirmed enum('y','n'));

create table Crony_Request(id int primary key auto_increment,Request_from int,FOREIGN KEY (Request_from) REFERENCES Person(Id),Requested_to int,FOREIGN KEY (Requested_to) REFERENCES Person(Id),Requested_Time datetime);

create table Crony_Ship(id int primary key auto_increment,Crony_Request_id int,FOREIGN KEY (Crony_Request_id ) REFERENCES Crony_Request(Id));

create table Groups(id int primary key auto_increment,Name varchar(70),admin int,FOREIGN KEY(admin) REFERENCES Person(Id));

create table Groups_Persons(s_no int primary key auto_increment,GroupId int,FOREIGN KEY(GroupId) REFERENCES Groups(Id),Person int,FOREIGN KEY(Person) REFERENCES Person(Id));

create table Wall_Poster(id int primary key auto_increment,Poster_Content enum('text','photo','video','url'),Pasted_Time datetime,Pasted_By int ,FOREIGN KEY (Pasted_By) REFERENCES Person(Id),Pasted_On enum('own_wall','group_wall'));

create table Gossip(id int primary key auto_increment,Gossiped_By int,FOREIGN KEY(Gossiped_By) REFERENCES Person(Id),Gossip longtext,Gossiped_To int,FOREIGN KEY(Gossiped_To) REFERENCES Person(Id),Gossip_Time datetime);

create table Group_Wall_Poster(id int primary key auto_increment,Wall_Poster_Id int,FOREIGN KEY(Wall_Poster_Id) REFERENCES Wall_Poster(Id));

create table Followers(id int primary key auto_increment,Followed int,FOREIGN KEY(Followed) REFERENCES Person(Id),Followed_By int,FOREIGN KEY(Followed_By) REFERENCES Person(Id));

create table Likes(id int primary key auto_increment,Given_By int,FOREIGN KEY(Given_By) REFERENCES Person(Id),Given_To int,FOREIGN KEY(Given_To) REFERENCES Wall_Poster(Id),Given_Time datetime);

create table Comments(id int primary key auto_increment,Commented_By int,FOREIGN KEY(Commented_By) REFERENCES Person(Id),Commented_on int,FOREIGN KEY(Commented_on) REFERENCES Wall_Poster(Id),Comment_Time datetime);

create table Shares(id int primary key auto_increment,Shared_By int,FOREIGN KEY(Shared_By) REFERENCES Person(Id),Poster int,FOREIGN KEY(Poster) REFERENCES Wall_Poster(Id),Shared_Time datetime);
